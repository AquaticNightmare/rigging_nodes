import warnings
import bpy
import uuid
from bpy.types import NodeTree
from .exceptions import BBN_NodeException
from .runtime import runtime_info, cache_node_times, cache_nodetree_times, cache_socket_conections, cache_node_group_outputs, cache_tree_portals, cache_socket_variables, cache_node_dependants, MeasureTreeTime


class BBN_created_object_temp(bpy.types.PropertyGroup):
    '''
    Temporal property group for storing objects created by nodes
    after the tree execution the objects will be added to the specific collection
    The group will be deleted and replaced by the one below which will be permjanently linked to the tree
    If another object with the same path already existed, it will be replaced by this new object
    Depending on the register data property, the old data will be removed too
    '''
    # the path to the node that created the object
    orig_node_path: bpy.props.StringProperty()
    # the created object
    value: bpy.props.PointerProperty(type=bpy.types.Object)
    # if true, the data datablock will be removed
    register_data: bpy.props.BoolProperty()
    # if true, the object will be added to the viewlayer
    add_to_collection: bpy.props.BoolProperty()
    # the collection the object will be placed in after the execution
    collection: bpy.props.StringProperty()

    mode: bpy.props.EnumProperty(items=[
        ('NOT_SELECTED', 'Not Selected', 'Not Selected'),
        ('OBJECT', 'Object', 'Object'),
        ('POSE', 'Pose', 'Pose'),
        ('EDIT', 'Edit', 'Edit'),
    ])


class BBN_preview_object(bpy.types.PropertyGroup):
    '''
    Temporal objects linked to the scene by a node to edit their datablock
    They are deleted before the tree execution
    '''
    # the object
    value: bpy.props.PointerProperty(type=bpy.types.Object)


class BBN_created_object(bpy.types.PropertyGroup):
    '''
    Permanently stores object information from the last executions
    the orig_node_path should be unique, it will be checked for replacing old objects with newly created ones
    '''
    # the path to the node that created the object
    orig_node_path: bpy.props.StringProperty()
    # the object
    value: bpy.props.PointerProperty(type=bpy.types.Object)
    # it means that the data of the object was created/modified by the tree and it can be deleted
    register_data: bpy.props.BoolProperty()


class BBN_tree_base():
    show_times: bpy.props.BoolProperty(default=False)
    auto_update: bpy.props.BoolProperty(name='Auto Execute', description='If active, the tree will be atomatically executed when a value in a node changes', default=True)

    show_outputs_in_ui: bpy.props.BoolProperty(default=True, name='Show Outputs')
    show_connected_inputs_in_ui: bpy.props.BoolProperty(default=False, name='Show Inputs')

    preview_node: bpy.props.StringProperty()

    # the object created by the nodetree
    # at the beginning of each execution they are removed from the viewlayer
    # the input nodes should take care of replacing the old references and adding them back to the viewlayer
    objects: bpy.props.CollectionProperty(type=BBN_created_object)
    temp_objects: bpy.props.CollectionProperty(type=BBN_created_object_temp)
    preview_object: bpy.props.PointerProperty(type=BBN_preview_object)
    object_index: bpy.props.IntProperty()

    def serialize_path(self, path):
        if path is None:
            return ''
        return '$$$$'.join(path)

    def deserialize_path(self, path):
        return path.split('$$$$')

    def get_node_from_path(self, path):
        node_tree = self
        for x in path[1:-1]:
            node_tree = node_tree.nodes.get(x).node_tree
        node = node_tree.nodes.get(path[-1])
        return node

    def unlink_object(self, index):
        '''Removes all the references of the object form the current tree. Next tree executions will not replace the object'''
        self.objects.remove(index)

    def get_other_socket(self, socket):
        '''
        Returns connected socket

        It takes O(len(nodetree.links)) time to iterate thought the links to check the connected socket
        To avoid doing the look up every time, the connections are cached in a dictionary
        The dictionary is emptied whenever a socket/connection/node changes in the nodetree
        '''
        # accessing links Takes O(len(nodetree.links)) time.
        _nodetree_socket_connections = cache_socket_conections.setdefault(self, {})
        _connected_socket = _nodetree_socket_connections.get(socket, None)

        if _connected_socket:
            return _connected_socket

        socket = socket
        if socket.is_output:
            while socket.links and socket.links[0].to_node.bl_rna.name == 'Reroute':
                socket = socket.links[0].to_node.outputs[0]
            if socket.links:
                _connected_socket = socket.links[0].to_socket
        else:
            while socket.links and socket.links[0].from_node.bl_rna.name == 'Reroute':
                socket = socket.links[0].from_node.inputs[0]
            if socket.links:
                _connected_socket = socket.links[0].from_socket

        cache_socket_conections[self][socket] = _connected_socket
        return _connected_socket

    def update(self):
        '''Called when the nodetree sockets or links change, socket pair cache is cleared here'''
        if not runtime_info['executing']:
            # print(f'UPDATING {self}')
            if self in cache_socket_conections:
                del cache_socket_conections[self]
                # print(f'{self.name} - cleared connections')
            if self in cache_node_group_outputs:
                del cache_node_group_outputs[self]
                # print(f'{self.name} - cleared group outputs')
            if self in cache_tree_portals:
                del cache_tree_portals[self]
                # print(f'{self.name} - cleared portals')
            if self in cache_node_dependants:
                del cache_node_dependants[self]
                # print(f'{self.name} - cleared dependants')
        else:
            print('TRIED TO UPDATE TREE, BUT ITS EXECUTING')
        # change the socket of the reroute nodes
        if hasattr(self, 'nodes'):
            for node in self.nodes:
                if node.bl_idname == 'NodeReroute':
                    connected = self.get_other_socket(node.inputs[0])
                    if connected and connected.bl_idname != 'NodeSocketVirtual' and connected.bl_idname != node.inputs[0].bl_idname:
                        new_input = node.inputs.new(connected.bl_idname, '')
                        new_input.init_from_socket(connected.node, connected)
                        new_output = node.outputs.new(connected.bl_idname, '')
                        new_output.init_from_socket(connected.node, connected)
                        self.relink_socket(node.inputs[0], new_input)
                        self.relink_socket(node.outputs[0], new_output)

                        node.inputs.remove(node.inputs[0])
                        node.outputs.remove(node.outputs[0])

    def relink_socket(self, old_socket, new_socket):
        '''Utility function to relink sockets'''
        if not old_socket.is_output and not new_socket.is_output and old_socket.links:
            self.links.new(old_socket.links[0].from_socket, new_socket)
            self.links.remove(old_socket.links[0])
        elif old_socket.is_output and new_socket.is_output and old_socket.links:
            links = list(old_socket.links[:])
            for link in links:
                self.links.new(new_socket, link.to_socket)
                # self.links.remove(link)

    def set_preview(self, node_name):
        '''Set the current active node and updates the colors of all nodes'''

        self.preview_node = node_name
        for node in self.nodes:
            if hasattr(node, 'update_colors'):
                node.update_colors()

    def clear_stored_datablocks(self):
        '''Removes all previously created objects from the scene'''
        for x in self.objects:
            if x.value:
                for collection in x.value.users_collection[:]:
                    collection.objects.unlink(x.value)

        self.clear_preview_object()

    def register_object(self, context, object, node_path, name='', data_name='', collection='', mode='OBJECT', register_data=True):
        '''
        Store an object created by a node inside the tree for later deletion in the next tree execution
        It also relinks old objects to the newly created ones after the whole tree is executed, instead of at the beginning, to avoid vertex groups being renamed when renaming bones
        '''
        node_path = self.serialize_path(node_path)
        try:
            original_data = next(x for x in self.objects if x.orig_node_path == node_path)
        except StopIteration:
            original_data = None

        if not data_name:
            data_name = name
        if original_data and original_data.value:
            original_data.value.name = 'OLD_' + name
            if original_data.register_data and original_data.value.data:
                original_data.value.data.name = 'OLD_' + data_name

        object.name = name
        if register_data:
            object.data.name = data_name

        if object.name not in context.scene.collection.objects:
            context.scene.collection.objects.link(object)
            if bpy.ops.object.mode_set.poll():
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
            if context.selected_objects:
                for x in context.selected_objects[:]:
                    x.select_set(False)
            object.select_set(True)
            context.view_layer.objects.active = object

        reg_obj = self.temp_objects.add()
        reg_obj.value = object
        reg_obj.collection = collection
        reg_obj.mode = mode
        reg_obj.register_data = register_data
        reg_obj.orig_node_path = node_path
        reg_obj.add_to_collection = True

    def remove_temp_object_from_collection(self, obj):
        try:
            register_data = next(x for x in self.temp_objects if x.value == obj)
        except StopIteration:
            return

        register_data.add_to_collection = False

    def clear_preview_object(self):
        if self.preview_object.value:
            bpy.data.objects.remove(self.preview_object.value, do_unlink=True)

    def register_preview(self, context, object):
        '''
        Register a temporal object that is meant to be deleted when the tree is executed
        '''
        self.clear_preview_object()

        if object.name not in context.scene.collection.objects:
            context.scene.collection.objects.link(object)
            if bpy.ops.object.mode_set.poll():
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
            if context.selected_objects:
                for x in context.selected_objects[:]:
                    x.select_set(False)
            object.select_set(True)
            context.view_layer.objects.active = object

        self.preview_object.value = object

    def unregister_object(self, context, object):
        try:
            val = next(i for i, x in enumerate(self.objects) if x.value == object)
            self.objects.remove(val)
        except StopIteration:
            pass

    def check_object_status(self):
        for reg_obj in self.objects:
            if reg_obj.value:
                if not reg_obj.value.select_get():
                    reg_obj.mode = 'NOT_SELECTED'
                else:
                    reg_obj.mode = reg_obj.value.mode

    def relink_objects_to_collections(self):
        '''
        Executed at the end of the node tree execution to append created objects to their layers
        And relink old objects to the newly created ones
        '''
        # TODO: this is wrong
        def recursive_search(layer):
            yield from layer.children

        bpy.ops.object.select_all(action='DESELECT')

        for register_data in self.temp_objects:
            try:
                original_data = next(x for x in self.objects if x.orig_node_path == register_data.orig_node_path)
            except StopIteration:
                original_data = None

            # remove and replace the old object with the new one
            if register_data.value:
                new_object = register_data.value
                if original_data and original_data.value:
                    original_object = original_data.value
                    original_object.user_remap(new_object)
                    if original_data.register_data:
                        old_data = original_object.data
                        if old_data:
                            original_object.data.user_remap(new_object.data)

                            if original_object.type == 'ARMATURE':
                                bpy.data.armatures.remove(old_data)
                            elif original_object.type == 'MESH':
                                bpy.data.meshes.remove(old_data)
                            elif original_object.type == 'CURVE':
                                bpy.data.curves.remove(old_data)
                            elif original_object.type == 'GPENCIL':
                                bpy.data.grease_pencils.remove(old_data)
                            else:
                                print('{} type not yet supported?'.format(original_object.type))

                    try:
                        bpy.data.objects.remove(original_object)
                    except ReferenceError:
                        print('data already deleted')

                    for collection in new_object.users_collection[:]:
                        collection.objects.unlink(new_object)

                # Add the object to a collection
                if register_data.add_to_collection:
                    coll = None
                    if register_data.collection:
                        coll = bpy.data.collections.get(register_data.collection)
                        if not coll:
                            coll = bpy.data.collections.new(register_data.collection)
                            bpy.context.scene.collection.children.link(coll)
                        elif coll not in recursive_search(bpy.context.scene.collection):
                            bpy.context.scene.collection.children.link(coll)
                    else:
                        coll = bpy.context.scene.collection

                    if new_object.name not in coll.objects:
                        coll.objects.link(new_object)

                    # Set the mode to the object
                    if register_data.mode != 'NOT_SELECTED':
                        new_object.select_set(True)
                        bpy.context.view_layer.objects.active = new_object
                        bpy.ops.object.mode_set(mode=register_data.mode)
                    else:
                        new_object.select_set(False)

        # replace the information, and if it does not exist, store it
        for register_data in self.temp_objects:
            try:
                original_data = next(x for x in self.objects if x.orig_node_path == register_data.orig_node_path)
            except StopIteration:
                original_data = self.objects.add()

            original_data.value = register_data.value
            original_data.orig_node_path = register_data.orig_node_path
            original_data.register_data = register_data.register_data

        self.temp_objects.clear()

    def execute(self, context):
        '''Executes the nodes in the tree'''

        cache_nodetree_times.clear()
        cache_socket_variables.clear()
        cache_node_times.clear()

        with MeasureTreeTime('Total'):
            runtime_info['executing'] = True

            # if the user is editing the metarig the armature will get corrupted, so first change it to object mode
            if (bpy.ops.object.mode_set.poll()):
                bpy.ops.object.mode_set(mode="OBJECT")
            context.space_data.node_tree.clear_stored_datablocks()

            id = str(uuid.uuid4())

            path = []

            try:
                # Create a temporal scene to work in to avoid slow edit/pose/object mode changes
                orig_scene = context.scene
                new_scene = bpy.data.scenes.new('__temp_rigging_scene__')
                context.window.scene = new_scene

                path.append(context.space_data.node_tree.name)
                # Execute all the parent trees first up to their active node
                with MeasureTreeTime('Tree Execution'):
                    for i in range(0, len(context.space_data.path) - 1):
                        node = context.space_data.path[i].node_tree.nodes.active
                        node.execute_dependants(context, id, path)

                        path.append(node.name)

                    node = self.nodes.get(self.preview_node)
                    if node:
                        node.execute(context, id, path)
            except BBN_NodeException as e:

                print(f"\n***************************\nNODE EXCEPTION\n{context.space_data.node_tree} -> {' -> '.join(e.path)} -> {e.node.name}\n***************************\n")
                print(e.info)
                print(e.traceback)

            finally:
                with MeasureTreeTime('Clean Up'):
                    context.space_data.node_tree.check_object_status()

                    if (bpy.ops.object.mode_set.poll()):
                        bpy.ops.object.mode_set(mode="OBJECT")

                    context.window.scene = orig_scene
                    bpy.data.scenes.remove(new_scene, do_unlink=True)

                    context.space_data.node_tree.relink_objects_to_collections()

                    # to update any animations applied to the armature, view_layer.update() doesn't work
                    context.scene.frame_set(context.scene.frame_current)
                    # print(f'FINISHED EXECUTING {self}')
                    runtime_info['executing'] = False

    def value_updated(self, context):
        '''Sockets and properties call this function for automatically executing the tree whenever a property changes'''
        if runtime_info['executing']:
            return
        if hasattr(self, 'do_not_update') and self.do_not_update or not self.id_data.auto_update:
            return
        if (hasattr(context.space_data, "node_tree") and context.space_data.node_tree.bl_idname == "bbn_tree"):
            tree = context.space_data.node_tree
            if tree.auto_update:
                context.space_data.edit_tree.execute(context)


class BBN_tree(NodeTree, BBN_tree_base):
    bl_idname = 'bbn_tree'
    bl_label = "Rigging nodes"
    bl_icon = 'OUTLINER_OB_ARMATURE'


class BBN_tree_group(NodeTree, BBN_tree_base):
    bl_idname = 'bbn_tree_group'
    bl_label = "Rigging nodes (groups)"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    @ classmethod
    def poll(cls, context):
        """Exclude this class from searching of node tree windows manager"""
        return False


class BBN_tree_loop(NodeTree, BBN_tree_base):
    bl_idname = 'bbn_tree_loop'
    bl_label = "Rigging nodes (loops)"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    @ classmethod
    def poll(cls, context):
        """Exclude this class from searching of node tree windows manager"""
        return False


classes = [
    BBN_preview_object,
    BBN_created_object_temp,
    BBN_created_object,
    BBN_tree,
    BBN_tree_group,
    BBN_tree_loop,
]


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)
