from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info


class BBN_node_branch(Node, BBN_node):
    bl_idname = 'bbn_branch_node'
    bl_label = "Branch (If / Else)"
    bl_icon = 'RIGHTARROW_THIN'

    input_sockets = {
        'Bool': {'type': 'BBN_bool_socket'},
        '...': {'type': 'BBN_add_array_socket'},
    }

    socket_type: bpy.props.StringProperty()

    def execute_dependants(self, context, id, path):
        connected_socket = self.inputs[0].connected_socket
        if connected_socket:
            x = connected_socket.node
            self.execute_other(context, id, path, x)
        if self.get_input_value(0):
            connected_socket = self.inputs[1].connected_socket
            if connected_socket:
                x = connected_socket.node
                self.execute_other(context, id, path, x)
        else:
            connected_socket = self.inputs[2].connected_socket
            if connected_socket:
                x = connected_socket.node
                self.execute_other(context, id, path, x)

    def process(self, context, id, path):
        if self.get_input_value(0):
            self.outputs[0].set_value(self.get_input_value(1))
        else:
            self.outputs[0].set_value(self.get_input_value(2))

    def update(self):
        if hasattr(self, 'initializing') and self.initializing:
            return
        if not self.can_update():
            return
        self.update_others()

        main_input = self.inputs[1]
        connected_socket = main_input.connected_socket

        if main_input.bl_idname == 'BBN_add_array_socket' and connected_socket:
            self.socket_type = connected_socket.bl_rna.name

            new_socket = self.inputs.new(connected_socket.bl_rna.name, 'True')._init()
            new_socket.init_from_socket(connected_socket.node, connected_socket)

            self.id_data.relink_socket(main_input, new_socket)

            new_socket = self.inputs.new(self.socket_type, 'False')._init()
            new_socket.init_from_socket(connected_socket.node, connected_socket)

            new_socket = self.outputs.new(self.socket_type, 'Out')._init()
            new_socket.init_from_socket(connected_socket.node, connected_socket)

            self.inputs.remove(main_input)

        self.remove_incorrect_links()
