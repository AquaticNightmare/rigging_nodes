import bpy
from bpy.types import Node

from ..node_base import BBN_node

from ...node_tree import BBN_tree

from mathutils import Vector


class BBN_node_set_mode(Node, BBN_node):
    bl_idname = 'bbn_set_mode_node'
    bl_label = "Set mode"

    input_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    object_mode: bpy.props.EnumProperty(items=[('EDIT', 'Edit', 'Edit Bone Properties'), ('POSE', 'Pose', 'Pose Bone Properties'), ('OBJECT', 'Object', 'Bone Properties')], update=BBN_tree.value_updated)

    input_to_focus = 'Object'

    @property
    def focus_mode(self):
        return {self.object_mode}

    def draw_buttons(self, context, layout):
        layout.prop(self, 'object_mode', text='')

        super().draw_buttons(context, layout)

    def process(self, context, id, path):
        self.outputs[0].set_value(self.get_input_value(0))
