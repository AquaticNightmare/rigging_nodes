import bpy
import operator
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info

operations = [
    ('==', '==', 'Equals'),
    ('<', '<', 'Less than'),
    ('<=', '<=', 'Less than or equal'),
    ('>', '>', 'Greater than'),
    ('>=', '>=', 'Greater than or equal'),
    ('!=', '!=', 'Not equal'),
]

operation_functions = {
    '==': operator.eq,
    '<': operator.lt,
    '<=': operator.le,
    '>': operator.gt,
    '>=': operator.ge,
    '!=': operator.ne,
}


class BBN_node_compare(Node, BBN_node):
    bl_idname = 'bbn_compare_node'
    bl_label = "Compare"
    bl_icon = 'SYNTAX_OFF'

    input_sockets = {
        'Val 1': {'type': 'BBN_add_array_socket'},
    }

    output_sockets = {
        'Result': {'type': 'BBN_bool_socket'},
    }

    compare_type: bpy.props.EnumProperty(items=operations)

    def compare(self, val1, val2):
        op = operation_functions[self.compare_type]
        return op(val1, val2)

    def process(self, context, id, path):
        val1 = self.inputs[0].get_real_value()
        val2 = self.inputs[1].get_real_value()

        op = operation_functions[self.compare_type]

        self.outputs[0].set_value(op(val1, val2))

    def draw_buttons(self, context, layout):

        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'compare_type', text='')

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                self.change_socket(self.inputs, 'Val 1', {'type': incoming_type})
                self.change_socket(self.inputs, 'Val 2', {'type': incoming_type})

        self.remove_incorrect_links()
