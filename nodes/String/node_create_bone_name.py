import bpy
from bpy.types import Node

from ..node_base import BBN_node
from ...preferences import preferences


def create_new_name(
        input_bone='',
        input_type=None,
        input_number=None,
        input_side=None,
        input_base_name=None,
        input_prefix=None,
        input_suffix=None,
        input_edit_suffix=None,
        input_edit_prefix=None,
):
    type_prefix = ''
    names = []
    side = ''
    number = ''

    splited = input_bone.rsplit('.', 1)
    if len(splited) == 2 and splited[1].isdigit():
        number = splited[1]
        input_bone = splited[0]

    orig_split = input_bone.split(preferences.separator)
    if orig_split:
        for x in reversed(orig_split):
            if not number and not names and x.isdigit():
                number = x
            elif not side and not names and x in preferences.side_flags.values():
                side = x
            elif not type_prefix and x in preferences.type_flags.values():
                type_prefix = x
            else:
                names.insert(0, x)

    if input_type is not None:
        type_prefix = preferences.type_flags[input_type]

    if input_number is not None:
        if input_number > 0:
            number = f'{input_number:03}'
        else:
            number = ''

    if input_side is not None:
        side = preferences.side_flags[input_side]

    if input_base_name is not None:
        names = [input_base_name]

    pre_names = names.copy()

    if input_edit_suffix is not None:
        if len(pre_names) > 1:
            names[-1] = input_edit_suffix
        else:
            names.append(input_edit_suffix)

    if input_edit_prefix is not None:
        if len(pre_names) > 1:
            names[0] = input_edit_prefix
        else:
            names.insert(0, input_edit_prefix)

    if input_prefix is not None:
        names.insert(0, input_prefix)

    if input_suffix is not None:
        names.append(input_suffix)

    name_array = [type_prefix] + names + [side]
    final_name = preferences.separator.join(x for x in name_array if x)
    if number:
        final_name = f'{final_name}.{number}'

    return final_name


class BBN_node_name_bone(Node, BBN_node):
    bl_idname = 'bbn_node_name_bone'
    bl_label = "Create Name"
    bl_icon = 'SYNTAX_OFF'

    bl_width_default = 250

    input_sockets = {

    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only affect one bone', '', 0),
        ('MULTIPLE', 'Multiple', 'Change property of multiple bones', '', 2),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'OUTPUTS': {
                'Name': {'type': 'BBN_string_socket'},
            },
            'INPUTS': {
                'Original Bone': {'type': 'BBN_string_socket'},
            },
        },
        'MULTIPLE': {
            'OUTPUTS': {
                'Name': {'type': 'BBN_string_array_v2_socket', 'array_socket_type': 'BBN_string_socket'},
            },
            'INPUTS': {
                'Original Bone': {'type': 'BBN_string_array_v2_socket', 'array_socket_type': 'BBN_string_socket'},
            },
        },
    }

    _opt_input_sockets = {
        'Type': {'type': 'BBN_enum_socket', 'items': [
            ('DEF', 'Deform', 'Deform', 'NONE', 0),
            ('TRGT', 'Target', 'Target', 'NONE', 1),
            ('MCH', 'Mechanism', 'Mechanism', 'NONE', 2),
            ('CTRL', 'Control', 'Control', 'NONE', 3),
            ('NONE', 'Clear', 'Clear', 'NONE', 4),
        ]},
        'Original Bone': {'type': 'BBN_string_socket'},
        'Base Name': {'type': 'BBN_string_socket'},
        'Prefix': {'type': 'BBN_string_socket'},
        'Suffix': {'type': 'BBN_string_socket'},
        'Edit Prefix': {'type': 'BBN_string_socket'},
        'Edit Suffix': {'type': 'BBN_string_socket'},
        'Side': {'type': 'BBN_enum_socket', 'items': [
            ('MID', 'Mid', 'Mid', 'NONE', 0),
            ('RIGHT', 'Right', 'Right', 'NONE', 1),
            ('LEFT', 'Left', 'Left', 'NONE', 2),
            ('NONE', 'Clear', 'Clear', 'NONE', 3),
        ]},
        'Number': {'type': 'BBN_int_socket'},
    }

    output_sockets = {
    }

    def init(self, context):
        super().init(context)

    def process(self, context, id, path):

        orig_bones = self.get_input_value('Original Bone', default='')

        if self.current_behaviour == 'SINGLE':
            orig_bones = [orig_bones]

        ans_bones = []

        input_type = self.get_input_value('Type', default=None)
        input_number = self.get_input_value('Number', default=None)
        input_side = self.get_input_value('Side', default=None)
        input_base_name = self.get_input_value('Base Name', default=None)
        input_prefix = self.get_input_value('Prefix', default=None)
        input_suffix = self.get_input_value('Suffix', default=None)
        input_edit_prefix = self.get_input_value('Edit Prefix', default=None)
        input_edit_suffix = self.get_input_value('Edit Suffix', default=None)

        for orig_bone in orig_bones:
            final_name = create_new_name(
                orig_bone,
                input_type,
                input_number,
                input_side,
                input_base_name,
                input_prefix,
                input_suffix,
                input_edit_suffix,
                input_edit_prefix,
            )

            ans_bones.append(final_name)

        if self.current_behaviour == 'SINGLE':
            self.set_output_value(0, ans_bones[0])
        elif self.current_behaviour == 'MULTIPLE':
            self.set_output_value(0, ans_bones)
