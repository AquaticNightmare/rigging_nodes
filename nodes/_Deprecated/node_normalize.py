import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_normalize(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_normalize_node'
    bl_label = "Normalize"
    bl_icon = 'EMPTY_SINGLE_ARROW'

    input_sockets = {
        'Vector': {'type': 'BBN_vector_socket'},
    }

    output_sockets = {
        'Normalized': {'type': 'BBN_vector_socket'},
    }

    def process(self, context, id, path):
        raise ValueError('This node is deprecated, replace it with the math node')
