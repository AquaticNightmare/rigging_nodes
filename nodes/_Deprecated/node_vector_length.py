from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_vector_length(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_vector_length_node'
    bl_label = "Vector Length"

    input_sockets = {
        'Vector': {'type': 'BBN_vector_socket'},
    }

    output_sockets = {
        'Length': {'type': 'BBN_float_socket'},
    }

    def process(self, context, id, path):
        raise ValueError('This node is deprecated, replace it with the math node')
