from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from .node_base import BBN_node
from ..node_tree import BBN_tree


class BBN_nodebase_input(BBN_node):
    subtype = 'INPUT'

    node_version = 1

    _opt_input_sockets = {
        'Name': {'type': 'BBN_string_socket'},
        'Collection': {'type': 'BBN_string_socket'},
    }

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    executable = False

    bl_width_default = 300

    def upgrade_node(self):
        super().upgrade_node()

        if 'created_object' in self.keys():
            del self['created_object']

        self.current_node_version = self.node_version

    def pre_process(self, context, id, path):
        if self.id_data.bl_idname not in {'bbn_tree', 'bbn_tree_group'}:
            raise ValueError('Object inputs are not allowed inside loops')
