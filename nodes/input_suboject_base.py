from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from .input_base import BBN_nodebase_input
from ..node_tree import BBN_tree


class BBN_nodebase_input_subobject(BBN_nodebase_input):
    subtype = 'INPUT'

    bl_idname = 'BBN_nodebase_input_subobject'
    bl_label = "Input Subobject Base"

    stored_datablock: bpy.props.PointerProperty(type=bpy.types.Armature)

    def draw_label(self):
        if self.stored_datablock:
            return self.stored_datablock.name

        return self.bl_label

    def socket_type_selected(self, context):
        if self.outputs[0].bl_rna.name != self.socket_type:
            self.outputs.remove(self.outputs[0])
            self.outputs.new(self.socket_type, 'Object')._init()

    socket_type: bpy.props.EnumProperty(
        items=[
            ('BBN_object_socket', 'Editable', 'Editable'),
            ('BBN_object_ref_socket', 'Reference', 'Reference')
        ],
        update=socket_type_selected
    )

    def create_datablock(self, context):
        self.stored_datablock = bpy.data.armatures.new(self.name)

    def draw_buttons(self, context, layout):
        super().draw_buttons(context, layout)

        if not self.stored_datablock:
            row = layout.row(align=True)
            row.prop(self, 'stored_datablock', text='')
            row.operator('bbn.new_datablock', text='New', icon=self.bl_icon).node = self.name
        if self.stored_datablock:
            col = layout.column(align=True)
            row = col.row(align=True)
            row.prop(self, 'stored_datablock', text='')
            row.operator('bbn.edit_datablock', text='', icon='OUTLINER_DATA_GP_LAYER').node = self.name
            col.prop(self, 'socket_type', text='')

        #super().draw_buttons(context, layout)

    def register_edit_object(self, context, obj):
        context.space_data.node_tree.register_preview(context, obj)

    def edit_datablock(self, context, clear_other_objects=True):
        if (bpy.ops.object.mode_set.poll()):
            bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.select_all(action="DESELECT")

        if clear_other_objects:
            self.id_data.clear_stored_datablocks()

        name = 'PREVIEW_' + self.stored_datablock.name
        new_obj = bpy.data.objects.new(name=name, object_data=self.stored_datablock)
        context.space_data.node_tree.register_preview(context, new_obj)

        new_obj.select_set(True)
        bpy.context.view_layer.objects.active = new_obj
        if not self.stored_datablock.library:
            if type(self.stored_datablock) != bpy.types.GreasePencil:
                bpy.ops.object.mode_set(mode="EDIT")
            else:
                bpy.ops.object.mode_set(mode="EDIT_GPENCIL")

    def process(self, context, id, path):
        super().process(context, id, path)
        if not self.stored_datablock:
            raise ValueError('No datablock selected')

        if (bpy.ops.object.mode_set.poll()):
            bpy.ops.object.mode_set(mode="OBJECT")

        bpy.context.view_layer.objects.active = None

        collection_name = self.get_input_value('Collection', default='')
        mode = self.get_input_value('Mode', default='OBJECT')

        result_obj = None

        input_name = self.get_input_value('Name', default='')
        if not input_name:
            input_name = self.stored_datablock.name
            data_name = 'OUT_' + input_name
        else:
            data_name = input_name

        if self.socket_type == 'BBN_object_socket':
            datablock = self.stored_datablock.copy()

            obj = bpy.data.objects.new(name=input_name, object_data=datablock)

            context.space_data.node_tree.register_object(context, obj, self.create_path_to_self(path), name=input_name, data_name=data_name, collection=collection_name, mode=mode, register_data=True)

            result_obj = obj
        else:
            obj = bpy.data.objects.new(name=input_name, object_data=self.stored_datablock)

            context.space_data.node_tree.register_object(context, obj, self.create_path_to_self(path), name=input_name, data_name=data_name, collection=collection_name, mode=mode, register_data=False)

            result_obj = obj

        self.set_output_value('Object', result_obj)
