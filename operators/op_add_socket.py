import bpy

last_node = None
last_possible_sockets = []
last_possible_sockets_32 = []
last_possible_sockets_64 = []
last_possible_sockets_96 = []
last_possible_sockets_128 = []


def sort_sockets(elem):
    return elem[1].get("display_name", elem[0])


def get_possible_sockets(node, var, sockets, search):
    global last_node
    global last_possible_sockets
    opt_sockets = getattr(node, var)
    last_node = node

    def can_be_added(socket, socket_info):
        if not search.lower() in socket.lower():
            return False
        for x in sockets:
            if (socket == x.name or x.name == socket_info.get('override_name', '')) and x.bl_rna.identifier == socket_info['type']:
                return False
        return True

    last_possible_sockets = [(key, item) for key, item in opt_sockets.items() if can_be_added(key, item)]
    last_possible_sockets.sort(key=sort_sockets)
    return last_possible_sockets


def sockets_to_enum(sockets, range1, range2):
    ans = [(
        sockets[i][0],
        f'{sockets[i][1].get("display_name", sockets[i][0])}',
        f'{sockets[i][1].get("description", sockets[i][0])}\n{sockets[i][0]}',
    ) for i in range(range1, min(len(sockets), range2))]
    return ans


var_to_check = {
    'opt_input_sockets': 'inputs',
    'opt_output_sockets': 'outputs'
}


def clear_cached_optional_sockets():
    global last_possible_sockets_32
    global last_possible_sockets_64
    global last_possible_sockets_96
    global last_possible_sockets_128

    last_possible_sockets_32 = []
    last_possible_sockets_64 = []
    last_possible_sockets_96 = []
    last_possible_sockets_128 = []


class BBN_OP_add_socket(bpy.types.Operator):
    '''Add optional socket'''
    bl_idname = "bbn.add_socket"
    bl_label = "Add Socket"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    socket_var: bpy.props.EnumProperty(items=[
        ('opt_input_sockets', 'Inputs', 'Inputs'),
        ('opt_output_sockets', 'Outputs', 'Outputs'),
    ])

    def search_updated(self, context):
        clear_cached_optional_sockets()

    search: bpy.props.StringProperty(options={'TEXTEDIT_UPDATE', 'SKIP_SAVE'}, update=search_updated)

    node: bpy.props.StringProperty()

    def get_sockets_32(self, context):
        global last_possible_sockets_32
        if last_possible_sockets_32:
            return last_possible_sockets_32
        node = context.space_data.edit_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        last_possible_sockets_32 = sockets_to_enum(sockets, 0, 32)
        return last_possible_sockets_32

    def get_sockets_64(self, context):
        global last_possible_sockets_64
        if last_possible_sockets_64:
            return last_possible_sockets_64
        node = context.space_data.edit_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        last_possible_sockets_64 = sockets_to_enum(sockets, 32, 64)
        return last_possible_sockets_64

    def get_sockets_96(self, context):
        global last_possible_sockets_96
        if last_possible_sockets_96:
            return last_possible_sockets_96
        node = context.space_data.edit_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        last_possible_sockets_96 = sockets_to_enum(sockets, 64, 96)
        return last_possible_sockets_96

    def get_sockets_128(self, context):
        global last_possible_sockets_128
        if last_possible_sockets_128:
            return last_possible_sockets_128
        node = context.space_data.edit_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        last_possible_sockets_128 = sockets_to_enum(sockets, 96, 128)
        return last_possible_sockets_128

    socket_32: bpy.props.EnumProperty(items=get_sockets_32, options={'ENUM_FLAG'})
    socket_64: bpy.props.EnumProperty(items=get_sockets_64, options={'ENUM_FLAG'})
    socket_96: bpy.props.EnumProperty(items=get_sockets_96, options={'ENUM_FLAG'})
    socket_128: bpy.props.EnumProperty(items=get_sockets_128, options={'ENUM_FLAG'})

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):

        global last_node
        node_tree = context.space_data.edit_tree
        node = node_tree.nodes.get(self.node)

        for socket in self.socket_32 | self.socket_64 | self.socket_96 | self.socket_128:
            socket_info = getattr(node, self.socket_var)[socket].copy()
            socket_info['is_deletable'] = True

            #getattr(node, var_to_check[self.socket_var]).new(getattr(node, self.socket_var)[socket]['type'], socket)._init(**socket_info)
            node.change_socket(getattr(node, var_to_check[self.socket_var]), socket, socket_info)

        self.socket_32, self.socket_64, self.socket_96, self.socket_128 = set(), set(), set(), set()
        last_node = None
        return {'FINISHED'}

    def invoke(self, context, event):
        global last_possible_sockets_32
        global last_possible_sockets_64
        global last_possible_sockets_96
        global last_possible_sockets_128

        last_possible_sockets_32 = []
        last_possible_sockets_64 = []
        last_possible_sockets_96 = []
        last_possible_sockets_128 = []

        wm = context.window_manager
        node_tree = context.space_data.edit_tree
        node = node_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        width = (1 + int(len(sockets) / 32)) * 150
        return wm.invoke_props_dialog(self, width=width)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, 'search')
        row = layout.row()
        col = row.column()
        col.prop(self, 'socket_32')
        col = row.column()
        col.prop(self, 'socket_64')
        col = row.column()
        col.prop(self, 'socket_96')
        col = row.column()
        col.prop(self, 'socket_128')
