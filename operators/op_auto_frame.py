from bpy.types import Operator
import bpy
from mathutils import Vector
import mathutils

from ..nodes import nodes_dict


colors = {
    'Armature': Vector((0.2, 0.2, 0.8)),
    'Array': Vector((0.2, 0.2, 0.2)),
    'Bone': Vector((0.2, 0.9, 0.2)),
    'Constants': Vector((0, 0, 0)),
    'Curve': Vector((0.2, 0.1, 1.0)),
    'Drivers': Vector((1, 0.1, 0.7)),
    'Flow_Control': Vector((0.2, 1.0, 0.6)),
    'Group': Vector((0.6, 0.6, 0.6)),
    'Inputs': Vector((0.2, 0.2, 0.2)),
    'Loop': Vector((0.6, 0.6, 0.6)),
    'Mesh': Vector((0.1, 0.3, 0.8)),
    'Object': Vector((0.1, 0.2, 0.7)),
    'Operations': Vector((0.8, 0.1, 0.1)),
    'Pointers': Vector((0.3, 0.3, 0.3)),
    'Python': Vector((0.8, 0.0, 0.8)),
    'String': Vector((0.8, 0.8, 0.6)),
}


def dist(a, b):
    return (mathutils.Vector(a) - mathutils.Vector(b)).length


def rect_distance(a, b, c, d):
    x1, y1 = a.x, a.y
    x1b, y1b = b.x, b.y
    x2, y2 = c.x, c.y
    x2b, y2b = d.x, d.y

    left = x2b < x1
    right = x1b < x2
    bottom = y2b < y1
    top = y1b < y2
    if top and left:
        return dist((x1, y1b), (x2b, y2))
    elif left and bottom:
        return dist((x1, y1), (x2b, y2b))
    elif bottom and right:
        return dist((x1b, y1), (x2, y2b))
    elif right and top:
        return dist((x1b, y1b), (x2, y2))
    elif left:
        return x1 - x2b
    elif right:
        return x2 - x1b
    elif bottom:
        return y1 - y2b
    elif top:
        return y2 - y1b
    else:             # rectangles intersect
        return 0


class BBN_OP_auto_frame(Operator):
    bl_idname = "bbn.auto_frame"
    bl_label = "Auto Frame"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    threshold: bpy.props.FloatProperty(default=150)

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        tree = context.space_data.edit_tree

        gathered_nodes = set()

        def get_category(node):
            try:
                ans = next(x for x, y in nodes_dict.items() if node.bl_rna.identifier in y)
                if ans not in colors.keys():
                    return None
            except StopIteration:
                return None
            return ans

        def check_distance(node, other, max=150):
            a = node.location
            b = node.location + mathutils.Vector((node.width, node.height))

            c = other.location
            d = other.location + mathutils.Vector((other.width, other.height))

            distance = rect_distance(a, b, c, d)
            return distance < max

        exclude_nodes = {'NodeFrame', 'NodeGroupInput', 'NodeGroupOutput'}
        all_nodes = [x for x in tree.nodes if x.bl_rna.identifier not in exclude_nodes and x.select]
        for node in all_nodes:
            if node.bl_rna.identifier == 'NodeFrame':
                #   tree.nodes.remove(node)
                continue
            if node not in gathered_nodes and not node.parent:
                nodes_to_check = [node]
                node_category = get_category(node)
                if node_category:
                    grouped = set()
                    checked = set()
                    while nodes_to_check:
                        check_node = nodes_to_check.pop(0)
                        if get_category(check_node) == node_category:
                            grouped.add(check_node)
                            gathered_nodes.add(check_node)

                            for other in all_nodes:
                                if other is not node and other not in checked and check_distance(node, other, max=self.threshold) and not other.parent and other not in gathered_nodes:
                                    nodes_to_check.append(other)

                            '''
                            for input in check_node.inputs:
                                if hasattr(input, 'connected_socket') and input.connected_socket:
                                    other = input.connected_socket.node
                                    if other not in checked and check_distance(node, other) and not other.parent:
                                        nodes_to_check.append(other)
                                        checked.add(other)
                            for output in check_node.outputs:
                                if hasattr(output, 'connected_socket') and output.connected_socket:
                                    other = output.connected_socket.node
                                    if other not in checked and check_distance(node, other) and not other.parent:
                                        nodes_to_check.append(other)
                                        checked.add(other)
                            '''

                    node_color = colors[node_category]
                    frame = tree.nodes.new("NodeFrame")
                    frame['__auto_generated__'] = 1
                    frame.label = node_category
                    frame.use_custom_color = True
                    frame.color = node_color
                    for node in grouped:
                        node.parent = frame

        return {'FINISHED'}
