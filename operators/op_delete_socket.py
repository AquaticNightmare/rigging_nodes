from bpy.types import Operator

import bpy


class BBN_OP_delete_socket(Operator):
    '''Delete the socket'''
    bl_idname = "bbn.delete_socket"
    bl_label = "Delete Socket"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()
    is_output: bpy.props.BoolProperty()
    current_index: bpy.props.IntProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        bone_tree = context.space_data.edit_tree

        node = bone_tree.nodes[self.node]
        sockets = node.outputs if self.is_output else node.inputs
        current_socket = sockets[self.current_index]

        sockets.remove(current_socket)

        return {'FINISHED'}
