from bpy.types import Operator
import bpy


class BBN_OP_focus_path_node(Operator):
    bl_idname = "bbn.focus_path_node"
    bl_label = "Focus Node"

    bl_options = {'INTERNAL'}

    path: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        context.space_data.path.clear()
        print(self.path)

        path = self.path.split('$$$$')
        print(path)
        current_tree = bpy.data.node_groups.get(path[0])
        print(current_tree)
        if not current_tree:
            self.report({'ERROR'}, 'Node not found')
            return {'CANCELLED'}
        context.space_data.node_tree = current_tree
        node = current_tree.nodes.get(path[1])
        if not node:
            self.report({'ERROR'}, 'Node not found')
            return {'CANCELLED'}
        bpy.ops.node.select_all(action='DESELECT')
        node.select = True
        current_tree.nodes.active = node

        for x in path[2:]:
            current_tree = node.node_tree
            node = current_tree.nodes.get(x)
            if not node:
                self.report({'ERROR'}, 'Node not found')
                return {'CANCELLED'}
            current_tree.nodes.active = node
            context.space_data.path.append(current_tree)

            bpy.ops.node.select_all(action='DESELECT')
            node.select = True

        def draw(self, context):
            self.layout.operator('bbn.focus_node').node = node.name

        bpy.context.window_manager.popup_menu(draw, title='Focus')

        return {'FINISHED'}


class BBN_OP_focus_error_node(Operator):
    bl_idname = "bbn.focus_error_node"
    bl_label = "Focus Node"

    bl_options = {'INTERNAL'}

    path: bpy.props.StringProperty()
    error_msg: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    @ classmethod
    def description(self, context, properties):
        return properties.error_msg

    def execute(self, context):
        bpy.ops.bbn.focus_path_node(path=self.path)

        return {'FINISHED'}
