from bpy.types import Operator
import bpy


class BBN_OP_new(Operator):
    """Creates a new Rigging Nodes default tree"""
    bl_idname = "bbn.new"
    bl_label = "New Rig Node Tree"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        space = context.space_data
        new_node_tree = bpy.data.node_groups.new("Rigging Nodes", "bbn_tree")
        new_node_tree.use_fake_user = True
        space.node_tree = new_node_tree

        input_armature_node = new_node_tree.nodes.new("bbn_input_armature_node")
        execute_node = new_node_tree.nodes.new("bbn_execute_node")

        input_armature_node.location = [-250, 70]
        execute_node.location = [200, 70]

        new_node_tree.links.new(input_armature_node.outputs[0], execute_node.inputs[0])

        bpy.ops.node.view_selected()

        return {"FINISHED"}
