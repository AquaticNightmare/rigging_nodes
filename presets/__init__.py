import os
import bpy
import pathlib
from bpy.types import (
    Operator,
    Menu,
)

from bpy.props import (
    StringProperty,
)

TEMPLATE_PREFIX = 'TEMPLATE_'

open_folder_icon = 'FILE_FOLDER'
if bpy.app.version >= (2, 83, 0):
    open_folder_icon = 'FOLDER_REDIRECT'

SEARCH_PATH = pathlib.Path(__file__).parent.resolve()


def node_center(context):
    from mathutils import Vector
    loc = Vector((0.0, 0.0))
    node_selected = context.selected_nodes
    if node_selected:
        for node in node_selected:
            loc += node.location
        loc /= len(node_selected)
    return loc


def node_template_add(context, filepath, node_group, ungroup, report):
    """ Main function
    """

    space = context.space_data
    node_tree = space.node_tree

    if node_tree is None:
        report({'ERROR'}, "No node tree available")
        return

    with bpy.data.libraries.load(filepath, link=False) as (data_from, data_to):
        if node_group not in data_from.node_groups:
            node_template_cache._node_cache.clear()
            return
        data_to.node_groups = [node_group]
    node_group = data_to.node_groups[0]

    # add node!aa

    path = context.space_data.path
    path.append(node_group)  # after appending, the user count of the node group is increased

    for node in node_group.nodes:
        node.select = True
    center = node_center(context)
    new_loc = context.space_data.cursor_location

    for node in node_group.nodes:
        node.location -= center
        node.location += new_loc

    bpy.ops.node.clipboard_copy()

    path.pop()

    bpy.ops.node.clipboard_paste()

    bpy.data.node_groups.remove(node_group, do_unlink=True)


class BBN_OT_template_add(Operator):
    """Add a node template"""
    bl_idname = "bbn.template_add"
    bl_label = "Add node group template"
    bl_description = "Add node group template"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    filepath: StringProperty(
        subtype='FILE_PATH',
    )
    group_name: StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree' and context.space_data.edit_tree

    def execute(self, context):
        node_template_add(context, self.filepath, self.group_name, True, self.report)

        return {'FINISHED'}

    def invoke(self, context, event):
        node_template_add(context, self.filepath, self.group_name, event.shift, self.report)

        return {'FINISHED'}


class BBN_OT_template_reload(Operator):
    """Reload template cache"""
    bl_idname = "bbn.template_reload"
    bl_label = "Reload node group templates"
    bl_description = "Reload node group templates"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    def execute(self, context):
        node_template_cache(context, reload=True)
        return {'FINISHED'}


# -----------------------------------------------------------------------------
# Node menu list

def node_template_cache(context, *, reload=False):
    if node_template_cache._node_cache_path != SEARCH_PATH:
        reload = True

    node_cache = node_template_cache._node_cache
    if reload:
        node_cache = []
    if node_cache:
        return node_cache

    for fn in os.listdir(SEARCH_PATH):
        if fn.endswith(".blend"):
            filepath = os.path.join(SEARCH_PATH, fn)
            with bpy.data.libraries.load(filepath) as (data_from, data_to):
                for group_name in data_from.node_groups:
                    if group_name.startswith(TEMPLATE_PREFIX):
                        node_cache.append((filepath, group_name))

    node_template_cache._node_cache = node_cache
    node_template_cache._node_cache_path = SEARCH_PATH

    return node_cache


node_template_cache._node_cache = []
node_template_cache._node_cache_path = ""


class BBN_MT_template_add(Menu):
    bl_label = "Rig Template"

    def draw(self, context):
        layout = self.layout

        node_items = node_template_cache(context)

        layout.operator("wm.path_open", text="Open Folder", icon=open_folder_icon).filepath = str(SEARCH_PATH)
        layout.operator(BBN_OT_template_reload.bl_idname, text="Reload", icon='FILE_REFRESH')

        for filepath, group_name in node_items:
            op = layout.operator(
                BBN_OT_template_add.bl_idname,
                text=group_name[len(TEMPLATE_PREFIX):],
            )
            op.filepath = filepath
            op.group_name = group_name


def add_node_button(self, context):
    if context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree':
        self.layout.menu(
            BBN_MT_template_add.__name__,
            text="Templates",
            icon='ARMATURE_DATA',
        )


classes = (
    BBN_OT_template_reload,
    BBN_OT_template_add,
    BBN_MT_template_add,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.NODE_MT_add.append(add_node_button)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

    bpy.types.NODE_MT_add.remove(add_node_button)
