import bpy
from bpy.types import NodeSocket, NodeSocketInterface
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_object_ref_base(BBN_socketmixin):
    color = (0.3, 1.0, 1.0, 1)
    shape = 'DIAMOND_DOT'

    default_value: bpy.props.PointerProperty(type=bpy.types.Object)


class BBN_socket_object_ref_interface(BBN_object_ref_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_object_ref_socket'


class BBN_socket_object_ref(BBN_object_ref_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_object_ref_socket'
    bl_label = "Object Ref"

    default_value: bpy.props.PointerProperty(type=bpy.types.Object, update=BBN_tree.value_updated)

    compatible_sockets = ['BBN_object_socket', 'BBN_pointer_socket']

    icon = 'OBJECT_DATAMODE'

    def convert_value(self, value):
        # If it's a pointer, get the datapath
        if type(value) is tuple and type(value[0]) is bpy.types.Object and type(value[1]) is str:
            return value[0]
        else:
            return value

    def value_to_string(self, value):
        if value:
            try:
                return f'{value.name}'
            except ReferenceError:
                return f'(REMOVED)'
        return '(NONE)'


class BBN_socket_object_ref_array_interface(BBN_array_base, BBN_object_ref_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_object_ref_array_socket'


class BBN_socket_object_ref_array(BBN_array_base, BBN_object_ref_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_object_ref_array_socket'
    bl_label = "Object Ref Array"

    socket_class = BBN_socket_object_ref
